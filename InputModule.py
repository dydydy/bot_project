import Crawl
import GetRandomItem
import HowManyPeople
import main
import scoring

######## 07.12수정 합시다.########################
'''
Database -> 
사용자 id, 채널, cnt, ans_num, 맨마지막 답시간

할일 1.
사용자별 타임스태프를 받아서 5분 이내 답을 안한 상태(0,1)[ 0: 초기상태 시작가능 / 1: 진행중 ]
모든 질문 응답시 상태 초기상태 0 변경
시작이란 단어 호출시 상태 1 로 변경 문제 뿌려줌
시작(상태가 진행중)후 이전 대답과 현재 대답의 시간 차이가 5분 이상시 처음부터 시작하라는 멘트 보내주고 상태 0으로 변경(모든 배열이나 cnt  초기화)

할일 2.
스코어링 부분 함수로 빼기

할일 3. 

초기 상태일때 시작 / 사용법 말고 다른 말 나올경우 처리
진행중 일때 숫자말고 다른 거 넣을 경우 처리
진행중 일때 '종료' 를 말할경우 상태 0으로 변경하고 모든 배열이나 cnt 초기화 

할일 4. (시간 남을경우)
문제 이쁘게 출력 

'''


def is_in_2min(old_ts, new_ts):
    old_ts = float(old_ts)
    new_ts = float(new_ts)

    if new_ts - old_ts < 120:
        return True
    else:
        return False


def _input(text, cnt, ans_num, dictionary, channel, ts):
    input_ = text.split()[1:]

    if dictionary[channel][0] == 0:  # 초기상태임으로 시작만 들어와야한다.
        if input_[0] in "시작":
            ret_text, man_list = GetRandomItem.getRandomItem(cnt, ans_num)
            for i in range(0, 4):
                dictionary[channel][3][i] = man_list[i]

            # database1.py 변경 할지 고민중
            #
            ################################

            # 초기상태 변경
            dictionary = main.change_state_dictionary(channel, dictionary)
            dictionary = main.change_cnt_dictionary(channel, dictionary)

            return ret_text, ''
        elif input_[0] in "사용법":
            return '''
            ****사용법****
            1. 테스트를 시작하시려면 "시작"이라고 입력해주세요.
            2. 문제에 대한 답은 오직 1~4의 숫자만 입력해주세요.
            3. 테스트 도중 종료를 원하시면 "종료"라고 입력해주세요.
            ****감사합니다****
            ''', ''
        else:
            return '"시작" 또는 "사용법" 이라고 입력해주세요', ''
    else:  # 진행중일때는 숫자 또는 종료만 들어와야함
        if is_in_2min(dictionary[channel][5], ts):
            if input_[0] in "종료":
                # 모든 배열및 변수들 초기화
                dictionary = main.set_init_dictionary(channel, dictionary)
                return "종료되었습니다.", ''
            elif input_[0].isdecimal():
                if int(input_[0]) in range(1, 5):
                    if cnt < 10:
                        dictionary[channel][2][dictionary[channel][3][int(input_[0]) - 1]] += 1  # 답한 질문에 대하여 스코어를 더함
                        ret_text, man_list = GetRandomItem.getRandomItem(cnt, ans_num)  # 랜덤 질문 받아오기

                        # 스코어링
                        for i in range(0, 4):
                            dictionary[channel][3][i] = man_list[i]

                        dictionary = main.change_cnt_dictionary(channel, dictionary)

                        print(dictionary[channel][3])
                        print(dictionary[channel][2])
                        return ret_text, ''
                    else:  # 마지막
                        dictionary[channel][2][dictionary[channel][3][int(input_[0]) - 1]] += 1  # 답한 질문에 대하여 스코어를 더함

                        # score_list[temp_list[int(input_[0]) - 1]] += 1
                        output = scoring._scoring(dictionary[channel][2])
                        text, img_url = Crawl._crawl(output)
                        # 몇명이 나와 같은 연예인이 나왔는지 -> 디비 필요
                        text += '\n당신과 같은 대답을 한 사람의 비율은 ' + HowManyPeople.howManyPeople(output,
                                                                                         "database.json") + " 입니다.\n"
                        dictionary = main.change_state_dictionary(channel, dictionary)
                        dictionary = main.set_init_dictionary(channel, dictionary)

                        return text, img_url
                else:
                    return "숫자(1~4) 또는 종료를 입력해주세요", ''

            else:
                return "숫자(1~4) 또는 종료를 입력해주세요", ''
        else:
            dictionary = main.set_init_dictionary(channel, dictionary)
            return "'2분간' 응답이 없어서 초기화 되었습니다.\n 처음부터 진행하여 주세요.", ''
