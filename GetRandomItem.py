import Questions as q_list
import Answers as a_list
from random import *
import IsSelected


def getRandomItem(cnt, ans_num):
    flag_num = 0
    for i_list in ans_num:
        if len(i_list) < 5:
            flag_num += 1

    if flag_num >= 4:
        i = 0
        man_num = []
        # 랜덤 4개 뽑아오고 랜덤기억
        full = "*["+str(cnt+1)+"번문제]* \n"+q_list.questions_list[0]+"\n"
        while i < 4:
            j = randint(0, 7)
            if j in man_num:
                continue
            else:
                if len(ans_num[j]) == 5:
                    continue
                man_num.append(j)
                k = randint(0, 4)
                while IsSelected.isSelected(j, k, ans_num):
                    k = randint(0, 4)
                ans_num[j].append(k)
                # i = 몇개 추출했는지 j = 사람 인덱스 k = 선택지 인덱스

            full = full + '\n ' + str(i + 1) + '. ' + str(list(a_list.answers_list[j].values())[0][k])
            i = i + 1
    else:
        print("not enough questions")
        full = "*["+str(cnt+1)+"번문제]* \n"+q_list.questions_list[0]+'\n'
        man_num = []
        sel_num = 0
        for i in range(0, 8):
            if len(ans_num[i]) != 5:
                selected_list = [0, 0, 0, 0, 0]
                for temp_num in ans_num[i]:
                    selected_list[temp_num] = 1
                    man_num.append(i)

                for s_i in range(0, 5):
                    if selected_list[s_i] == 0:
                        ans_num[i].append(s_i)
                        full = full + '\n ' + str(sel_num + 1) + '. ' + str(list(a_list.answers_list[i].values())[0][s_i])
                        sel_num += 1
            if sel_num == 4:
                break

    print(ans_num)
    return full, man_num
