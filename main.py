# -*- coding: utf-8 -*-

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
import InputModule

###################### Lee Token #######################################
# SLACK_TOKEN = 'token'
# SLACK_SIGNING_SECRET = 'token'
############################################################################

###################### Cho Token ########################################
SLACK_TOKEN = 'token'
SLACK_SIGNING_SECRET = 'token'
############################################################################

########GOBAL######################
init_dict = {"key = channel 명": [0, [[], [], [], [], [], [], [], []], [0,0,0,0,0,0,0,0], [0, 0, 0, 0], 0, 0.0]}
###################################


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)
'''
{"key = channel 명": ["state : 0(초기) / 1(진행중)","[ans_num]","[score_list]","[temp_list]","cnt","시간"]}
'''


def is_first(channel, dictionary):
    if channel in dictionary.keys():
        return False
    else:
        return True


def make_channel(channel, ts, dictionary):
    dictionary[channel] = [0, [[], [], [], [], [], [], [], []], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0], 0, float(ts)]


def change_state_dictionary(channel, dictionary):
    if dictionary[channel][0] == 0:
        dictionary[channel][0] = 1
        print("change 1")
    else:
        dictionary[channel][0] = 0
        print("change 0")
    return dictionary


def change_ts_dictionary(channel, dictionary, ts):
    dictionary[channel][5] = float(ts)


def change_cnt_dictionary(channel, dictionary):
    dictionary[channel][4] += 1
    return dictionary


def set_init_dictionary(channel, dictionary):
    dictionary[channel] = [0, [[], [], [], [], [], [], [], []], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0], 0, 0.0]
    return dictionary


def process_init(channel, ts):
    global init_dict
    print("len : " + str(len(init_dict)))
    print(list(init_dict.keys()))

    if is_first(channel, init_dict):
        print("first")
        make_channel(channel, ts, init_dict)
    else:
        print("another")


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    ts = event_data["event"]["event_ts"]

    global init_dict
    process_init(channel, ts)
    channel_list = list(init_dict[channel])

    message, img_url = InputModule._input(text, channel_list[4], channel_list[1], init_dict, channel, ts)

    # 마지막 응답시간변경, cnt 변경
    change_ts_dictionary(channel, init_dict, ts)
    message_list = message.split('\n')

    if len(img_url) > 0:
        slack_web_client.chat_postMessage(
            channel=channel,
            attachments=[
                {
                    "pretext": "당신의 성향과 닮은 연예인",
                    "title": "나무위키에서 자세히 보기",
                    "title_link": message_list[1],
                    "author_name": message_list[0],
                    "fields": [
                        {
                            "title": message_list[2]
                        }
                    ],
                    "image_url": img_url
                }
            ])
    else:
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )
        # if len(message_list) == 1:
        #     print("there")
        #     slack_web_client.chat_postMessage(
        #         channel=channel,
        #         text=message_list[0]
        #     )
        # else:
        #     print("here : "+str(len(message_list)))
        #     message = message[len(message_list[0]):]
        #     slack_web_client.chat_postMessage(
        #         channel=channel,
        #         attachments=[
        #             {
        #                 "pretext": message_list[0],
        #                 "title": message,
        #                 "fields": [
        #                     {
        #                         "title": message_list[1:]
        #                     }
        #                 ]
        #             }
        #         ]
        #     )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)  # 외부로 보내기 위한  0.0.0.0
