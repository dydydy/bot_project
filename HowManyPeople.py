from JsonHelper import JsonHelper


def howManyPeople(key, filename):
    # 파일오픈
    jsonhelper = JsonHelper(filename)
    dictionary = jsonhelper.create_dict()

    # 오픈된 내용 처리
    dictionary[key] += 1

    total_num = sum(dictionary.values())
    # for key, value in dictionary.items():
    #     total_num += value
    print(str(dictionary[key])+" "+str(total_num))
    percent_num = int(float(dictionary[key] / total_num) * 100)

    # 파일 쓰기
    jsonhelper.create_json(dictionary)

    return str(percent_num) + "%"
