import Answers
import random
import re

def _scoring(score_list):
    # 최고 스코어를 뽑아옴
    max_score = max(score_list)
    max_score_list = []
    for i in range(0, 8):
        if max_score == score_list[i]:
            max_score_list.append(i)

    result = random.choice(max_score_list)
    res_str = str(Answers.answers_list[result].keys())
    print(res_str)
    output = re.compile("\S+['](\S+)'").search(res_str).group(1)
    print(output)
    return output
    # 스코어링 끝, 결과값 output string
