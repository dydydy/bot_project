from bs4 import BeautifulSoup
from urllib import parse
import requests


def _crawl(name):
    # 여기에 함수를 구현해봅시다.
    # <@UL95BSCQ4> music
    # https://namu.wiki/w/%EA%B0%95%ED%98%B8%EB%8F%99

    # 크롤링 막힌곳 header 추가해서 가능하게 해줌
    headers = {'User-Agent': 'Mozilla/5.0'}
    name_t = name

    if name in "김민희":
        name_t = name_t+"(1982)"
    elif name in "길":
        name_t = name_t+"(리쌍)"
    name_t = parse.quote(name_t)
    url_tmp = "namu.wiki/w/" + name_t
    url2_tmp = "search.naver.com/search.naver?sm=top_hty&fbm=1&ie=utf8&query="
    # 한글 url 변경
    url = "http://" + url_tmp
    name = parse.quote(name)
    url2 = "http://" + url2_tmp + name
    # header 추가
    res = requests.get(url, headers=headers)
    soup = BeautifulSoup(res.text, "html.parser")
    # img_url 받아오는 부분
    res1 = requests.get(url2, headers=headers)
    soup2 = BeautifulSoup(res1.text, "html.parser")

    result = soup.find("span", class_="wiki-document-title").get_text()
    img_url = soup2.find("div", class_="big_thumb").find("img").get("src")
    print("img_url1 : " + img_url)
    result += "\nhttp://" + url_tmp
    return result, img_url
