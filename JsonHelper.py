import json


class JsonHelper:
    def __init__(self, filename):
        self.filename = filename

    def create_dict(self):
        '''
        JSON 파일을 읽고 문자열을 딕셔너리로 변환합니다.
        '''

        with open(self.filename, encoding='UTF-8-sig') as file:
            json_string = file.read()
            # 함수를 완성하세요.

            return json.loads(json_string)  # json 형태의 파일을 딕셔너리 형태로 파싱하는 라이브러리
            # return {"강호동":10000,
            #         "토마토":10000}

    def create_json(self, dictionary):
        '''
        JSON 파일을 읽고 딕셔너리를 JSON 형태의 문자열로 변환합니다.
        '''

        with open(self.filename, 'w', encoding='UTF-8-sig') as file:  # 'w' 쓰기 모드로 여는 것

            # 함수를 완성하세요.
            json_string = json.dumps(dictionary, ensure_ascii=False)
            file.write(json_string)
